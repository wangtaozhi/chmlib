Name:		chmlib
Summary:	Library for dealing with ITSS/CHM format files
Version:	0.40
Release:	1
License:	LGPLv2+
Url:		http://www.jedrea.com/chmlib/
Source0:	http://www.jedrea.com/chmlib/%{name}-%{version}.tar.bz2
# backported from upstream
Patch1:		chmlib-0001-Patch-to-fix-integer-types-problem-by-Goswin-von-Bre.patch
# backported from upstream
Patch2:		chmlib-0002-Fix-for-extract_chmLib-confusing-empty-files-with-di.patch
# Submitted upstream https://github.com/jedwing/CHMLib/pull/10
Patch3:		chm_http-port-shortopt.patch
# Submitted upstream https://github.com/jedwing/CHMLib/pull/11
Patch4:		chm_http-bind-localhost.patch
# Submitted upstream https://github.com/jedwing/CHMLib/pull/12
Patch5:		chm_http-output-server-address.patch
Patch6:         chmlib-c99.patch
BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	libtool
BuildRequires:  make


%description
CHMLIB is a library for dealing with ITSS/CHM format files. Right now, it is
a very simple library, but sufficient for dealing with all of the .chm files
I've come across. Due to the fairly well-designed indexing built into this
particular file format, even a small library is able to gain reasonably good
performance indexing into ITSS archives.


%package devel
Summary:	Library for dealing with ITSS/CHM format files - development files
Requires:	%{name}%{?_isa} = %{version}-%{release}


%description devel
Files needed for developing apps using chmlib.


%prep
%setup -q
%patch1 -p1 -b .types
%patch2 -p1 -b .files_dirs
%patch3 -p1 -b .shortopt
%patch4 -p1 -b .localhost
%patch5 -p1 -b .printaddr
%patch6 -p1
rm -f libtool
mv configure.in configure.ac
autoreconf -ivf


%build
%configure --enable-examples --disable-static
make %{?_smp_mflags}


%install
make DESTDIR=%{buildroot} install
rm -f %{buildroot}/%{_libdir}/*.la


%ldconfig_scriptlets


%files
%{_bindir}/chm_http
%{_bindir}/enum_chmLib
%{_bindir}/enumdir_chmLib
%{_bindir}/extract_chmLib
%{_bindir}/test_chmLib
%{_libdir}/libchm.so.*
%doc README AUTHORS COPYING NEWS


%files devel
%{_includedir}/*
%{_libdir}/libchm.so


%changelog
* Wed May 10 2023 peijiankang <peijiankang@kylinos.cn> - 0.40-1
- Init Package

